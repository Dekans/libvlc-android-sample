package com.videolabs.record

import android.net.Uri
import android.os.Build
import android.os.Environment
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.MutableLiveData
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_fullscreen.*
import kotlinx.coroutines.*
import org.videolan.libvlc.LibVLC
import org.videolan.libvlc.MediaPlayer
import java.util.concurrent.Executors

@ExperimentalCoroutinesApi
class PlayerDelegate(private val activity: FullscreenActivity) : CoroutineScope by activity, MediaPlayer.EventListener {

    private lateinit var mediaplayer: MediaPlayer
    private val playerContext = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
    val recordingDirectoryPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).canonicalPath
    private var recording = false
    val savedPath = MutableLiveData<String>()

    suspend fun setup() {
        var visibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        if (Build.VERSION.SDK_INT >= 19) visibility = visibility or View.SYSTEM_UI_FLAG_IMMERSIVE
        activity.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        activity.window.decorView.systemUiVisibility = visibility
        val libvlc = withContext(Dispatchers.IO) { LibVLC(activity.applicationContext) }
        withContext(playerContext) { mediaplayer = MediaPlayer(libvlc) }
        mediaplayer.setEventListener(this)
        mediaplayer.attachViews(activity.videolayout, null, false, false)
        mediaplayer.updateVideoSurfaces()
        activity.record_btn.setOnClickListener { toggleRecord() }
    }

    fun release() = launch(playerContext) {
        if (recording) stopRecording()
        mediaplayer.setEventListener(null)
        mediaplayer.release()
        activity.cancel()
    }

    internal fun toggleRecord() {
        if (recording) stopRecording()
        else startRecording(recordingDirectoryPath)
    }

    private fun startRecording(directory: String) = launch {
        recording = withContext(playerContext) { mediaplayer.record(directory) }
        if (recording) activity.record_btn.setImageResource(R.drawable.ic_record_on)
    }

    private fun stopRecording() = launch {
        withContext(Dispatchers.Main) { mediaplayer.record(null) }
        recording = false
    }

    fun start() = launch {
        if (!this@PlayerDelegate::mediaplayer.isInitialized) setup()
        withContext(playerContext) {
            mediaplayer.play(Uri.parse("https://streams.videolan.org/streams/mp4/GHOST_IN_THE_SHELL_V5_DOLBY%20-%203.m4v"))
        }
    }

    fun stop() = launch {
        if (!this@PlayerDelegate::mediaplayer.isInitialized) return@launch
        withContext(playerContext) { mediaplayer.stop() }
    }

    fun updateSize() = mediaplayer.updateVideoSurfaces()

    override fun onEvent(event: MediaPlayer.Event?) {
        when (event?.type) {
            MediaPlayer.Event.RecordChanged -> savedPath.value = event.recordPath
        }
    }
}