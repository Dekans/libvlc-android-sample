package com.videolabs.record

import android.content.res.Configuration
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.videolabs.record.StoragePermissionHelper.Companion.askStoragePermission
import kotlinx.android.synthetic.main.activity_fullscreen.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.MainScope

@ExperimentalCoroutinesApi
class FullscreenActivity : AppCompatActivity(), CoroutineScope by MainScope(), Observer<String> {

    private lateinit var playerDelegate: PlayerDelegate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen)
        playerDelegate = PlayerDelegate(this)
        askStoragePermission()
        playerDelegate.savedPath.observe(this, this)
    }

    override fun onStart() {
        super.onStart()
        playerDelegate.start()
    }

    override fun onStop() {
        super.onStop()
        playerDelegate.stop()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        playerDelegate.updateSize()
    }

    override fun onDestroy() {
        super.onDestroy()
        playerDelegate.release()
    }

    override fun onChanged(path: String?) {
        if (path !== null && !isFinishing) {
            record_btn.setImageResource(R.drawable.ic_record_off)
            Toast.makeText(this, "Video saved in $path".trim(), Toast.LENGTH_LONG).show()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_DPAD_CENTER -> playerDelegate.toggleRecord()
            else -> return super.onKeyDown(keyCode, event)
        }
        return true
    }
}
