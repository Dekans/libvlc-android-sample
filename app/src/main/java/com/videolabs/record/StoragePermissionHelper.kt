package com.videolabs.record

import android.Manifest
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import org.videolan.libvlc.util.AndroidUtil


class StoragePermissionHelper : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        val permissions = if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE)
        else arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        requestPermissions(permissions, 42)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode != 42) return
        val queue = mutableListOf<String>()
        for ((index, result) in grantResults.withIndex()) {
            if (result != PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(permissions[index])) {
                    queue.add(permissions[index])
                }
            }
        }
        if (!queue.isEmpty()) requestPermissions(queue.toTypedArray(), 42)
        else exit()
    }

    private fun exit() {
        retainInstance = false
        activity?.run {
            if (!isFinishing) supportFragmentManager
                .beginTransaction()
                .remove(this@StoragePermissionHelper)
                .commitAllowingStateLoss()
        }
    }

    companion object {
        fun FragmentActivity.askStoragePermission() {
            if (!AndroidUtil.isMarshMallowOrLater ||
                ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) return;
            supportFragmentManager.beginTransaction().add(StoragePermissionHelper(), "perms").commitAllowingStateLoss()
        }
    }
}